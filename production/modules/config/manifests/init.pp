class config {

  file { '/etc/puppetlabs/puppet/puppet.conf':
    ensure => file,
    source => 'puppet:///modules/config/puppet.conf',
    owner => 'root',
    mode => '0644'
  }

}
