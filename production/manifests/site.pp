node 'default' {
  include config
  include prometheus::server
  class { 'zabbix::agent':
    server => 'zabbix.lab.com',
  }
}

node 'devops.lab.com' {
  include primeiro
}
